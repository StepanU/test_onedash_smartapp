function onedash(token) {
    function wrapped(context){
        return $http.post('https://e6e31231bc2f.ngrok.io/smartapp/record',
        {
            body: {
                "context": context
            },
            headers: { 'Authorization': token }
        })
    }
    return wrapped;
}
