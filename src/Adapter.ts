import axios from "axios";

import {insertRecordUrl} from "./config";

export class OneDashAdapter {
    constructor(
        private readonly token: string
    ) {
    }

    async save(context) {
        const headers = {'Authorization': this.token}
        try {
            return axios.post(insertRecordUrl, context, {headers})
        } catch (e) {
            return e
        }

    }

}